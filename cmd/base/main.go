package main

import "../../internal/incrementor"

func init() {}
func main() {
	inc := incrementor.NewIncrementor()
	inc.IncrementNumber()
	println(inc.GetNumber())
}
