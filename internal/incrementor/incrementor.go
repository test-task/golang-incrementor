package incrementor

import "errors"

// Incrementor  ...
type Incrementor struct {
	Number int
	MaxValue int
}
// NewIncrementor create default struct
func NewIncrementor() *Incrementor {
	return &Incrementor{
		Number: 0,
		MaxValue: 1,
	}
}
// GetNumber returns Number
func (i *Incrementor) GetNumber() int {
	return i.Number
}

// IncrementNumber increments Number by 1
func (i *Incrementor) IncrementNumber() {
	if i.Number < i.MaxValue {
		i.Number = i.Number + 1
	} else {
		i.Number = 0
	}
}

// SetMaximumValue set MaxValue
func (i *Incrementor) SetMaximumValue(maximumValue int) error  {
	if maximumValue <= 0 {
		return errors.New("maximumValue must be greater then 0")
	}
	i.MaxValue = maximumValue
	return nil
}