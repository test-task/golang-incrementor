package incrementor

import (
	"testing"
)

const (
	defaultMaxValueRight int = 3
	defaultMaxValueWrong int = 0
)
// TestIncrementor_SetMaximumValue_Success ...
func TestIncrementor_GetNumber(t *testing.T) {
	inc := NewIncrementor()
	getNumber :=  inc.GetNumber()
	if getNumber != inc.Number{
		t.Error(
			"expected", inc.Number,
			"got", getNumber,
		)
	}
}

// TestIncrementor_SetMaximumValue_Success ...
func TestIncrementor_SetMaximumValue_Success(t *testing.T) {
	inc := NewIncrementor()
	err := inc.SetMaximumValue(defaultMaxValueRight)
	if err != nil {
		t.Error(
			"expected", nil,
			"got", err,
		)
	}
	if inc.MaxValue != defaultMaxValueRight {
		t.Error(
			"expected", defaultMaxValueRight,
			"got", inc.MaxValue,
		)
	}
}

// TestIncrementor_SetMaximumValue_Error ...
func TestIncrementor_SetMaximumValue_Error(t *testing.T) {
	inc := NewIncrementor()
	err := inc.SetMaximumValue(defaultMaxValueWrong)
	if err == nil {
		t.Error(
			"expected error",
			"got", nil,
		)
	}
}
// TestIncrementor_IncrementNumber ...
func TestIncrementor_IncrementNumber(t *testing.T) {
	inc := NewIncrementor()
	exp := 1 + inc.Number
	inc.IncrementNumber()
	if exp != inc.Number {
		t.Error(
			"expected", exp,
			"got", inc.Number,
		)
	}
}